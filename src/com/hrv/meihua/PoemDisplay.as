﻿package com.hrv.meihua {
	
	//import gs.TweenGroup;
	//import gs.TweenLite;
	import com.greensock.*;
	import com.greensock.easing.Linear;
	import flash.text.TextField;
	import flash.text.Font;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	import flash.display.Sprite;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.display.MovieClip;
	import flash.system.LoaderContext;
	import flash.system.ApplicationDomain;
	import flash.net.URLRequest;
	import flash.display.LoaderInfo;
	
	public class PoemDisplay extends Sprite {

		[Embed(source = "../../../Resource/邢体草书繁体.ttf", fontName = "xingticaoshu", embedAsCFF = "false", unicodeRange = "U+5899, U+89d2, U+6570, U+679d, U+6885, U+51cc, U+5bd2, U+72ec, U+81ea, U+5f00, U+9065, U+77e5, U+4e0d, U+662f, U+96ea, U+4e3a, U+6709, U+6697, U+9999, U+6765")]
		public var xingticaoshuFont:Class;
		
		private var poem:String = "墙角数枝梅凌寒独自开 遥知不是雪为有暗香来";
		private var signet:MovieClip;		// 右上角的“印章”
		private var signetDown:MovieClip;	// 左下角的“印章”
		private var poemSwf:Loader = new Loader();
		private var tweenLiteArray:Array = new Array();
		
		public var tweenGroup:TimelineMax;	// 动画序列组
		public var totalTime:Number = 0;	// 诗动画运行总时长
		
		public function PoemDisplay() {
			// constructor code
			// 动画组
			tweenGroup = new TimelineMax();
			
			poemSwf.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete); 
			var context:LoaderContext = new LoaderContext();  
			context.applicationDomain = new ApplicationDomain();  
			/*开始加载swf*/  
			poemSwf.load(new URLRequest("meihua/Poem.swf"), context); 
		}
		
		private function onLoadComplete(e:Event):void {
			var domain:ApplicationDomain = (e.currentTarget as LoaderInfo).applicationDomain;
			var cls:Class = domain.getDefinition("Signet") as Class;
			this.signet = new cls() as MovieClip;
			
			cls = domain.getDefinition("SignetDown") as Class;
			this.signetDown = new cls() as MovieClip;
			
			var formatter:TextFormat = new TextFormat();
			formatter.bold = true; // 设置粗体
			//formatter.color = 0xFF0000; // 设置文本颜色为黄色
			formatter.blockIndent = 5; //调整间距为5
			formatter.font = "xingticaoshu";
			formatter.size = 40;
			
			var fieldY:int = 0;
			var fieldX:int = 100;
			for (var i:int = 0; i < poem.length; i++) {
				if (poem.charAt(i) == " ") {
					fieldY = 0;
					fieldX = 50;
					continue;
				}
				var field:TextField = new TextField();
				field.autoSize = TextFieldAutoSize.LEFT;
				field.text = poem.charAt(i);
				field.selectable = false;
				field.embedFonts = true;
				field.setTextFormat(formatter);
				field.y = fieldY;
				field.x = fieldX;
				field.alpha = 0;
				addChild(field);
				fieldY += field.height - 10;
				// 
				var delay:Number = 1.8;
				//var tween:TweenLite = new TweenLite(field, delay, {alpha:1});
				//tweenLiteArray.push(tween);
				tweenGroup.append(TweenMax.to(field, delay, {alpha:1}));
				totalTime += delay;
			}
			
			/// 添加印章
			signetDown.x = 5;
			signetDown.y = 392;
			signetDown.alpha = 0;
			addChild(signetDown);
			var signetDownDelay:Number = 2.5;
			tweenGroup.append(TweenMax.to(signetDown, signetDownDelay, {alpha:1}));
			//tweenLiteArray.push(new TweenLite(signetDown, signetDownDelay, {alpha:1}));
			totalTime += signetDownDelay;
			
			signet.x = 155;
			signet.y = -2;
			signet.alpha = 0;
			signet.scaleX = 2.0;
			signet.scaleY = 2.0;
			addChild(signet);
			var signetDelay:Number = 3;
			//tweenLiteArray.push(new TweenLite(signet, signetDelay, {alpha:1, scaleX:1, scaleY:1}));
			tweenGroup.append(TweenMax.to(signet, signetDelay, {alpha:1, scaleX:1, scaleY:1}));
			totalTime += signetDelay;
			
			// 动画组
			//tweenGroup = new TweenGroup(tweenLiteArray);
			//tweenGroup.pause();
			//tweenGroup.align = TweenGroup.ALIGN_SEQUENCE;
			//tweenGroup.onComplete = doComplete;
			
			tweenGroup.pause();
			//tweenGroup.stop();
		}
		
		/*private function doComplete():void {
			tweenGroup.reversed = true;
		}*/

	}
	
}
