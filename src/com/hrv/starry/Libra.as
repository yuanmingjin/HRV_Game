﻿package com.hrv.starry {
	import com.greensock.*;
	import com.greensock.easing.Linear;
	import flash.display.*;
	
	/** 
	 * 天秤座
	 * 
	 * 星座动画的主线、动画序列包含“星星间连线的动画”，以及“星座图”出现或消失的动画
	 * 根据星星间连线的动画进度，增加或删除“闪烁的星星”元件
	 * 
	 */
	public class Libra extends Constellation {
		/**
		 * @param starCls 星星类
		 * @param libraCls 天秤座图像类
		 */ 
		public function Libra(starCls:Class, libraCls:Class) {
			// constructor code
			super(starCls, libraCls, 
				[{x:712, y:162}, {x:845, y:176}, {x:928, y:247}, {x:866, y:411}, {x:683, y:320}],
				[0, 0.2, 3, 6, 12]
			);
		}
		
		// 开始动画
		protected override function beginStarLine():void {
			// 添加星星连线动画
			addStarLine(new StarLine(_points[0], _points[1]));
			addStarLine(new StarLine(_points[1], _points[2]));
			addStarLine(new StarLine(_points[2], _points[3]));
			addStarLine(new StarLine(_points[2], _points[0]));
			addStarLine(new StarLine(_points[0], _points[4]));
			
		}

	}
	
}
