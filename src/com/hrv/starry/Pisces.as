﻿package com.hrv.starry {
	import com.greensock.*;
	import com.greensock.easing.Linear;
	import flash.display.*;
	
	/** 
	 * 双鱼座
	 * 
	 */
	public class Pisces extends Constellation {

		public function Pisces(starCls:Class, piscesCls:Class) {
			// constructor code
			super(starCls, piscesCls, 
				[
					{x:773, y:177}, 
					{x:698, y:240}, 
					{x:620, y:259}, 
					{x:563, y:304}, 
					{x:502, y:364}, 
					{x:640, y:368}, 
					{x:727, y:373}, 
					{x:811, y:366}, 
					{x:815, y:297}, 
					{x:741, y:315}
				],
				[0, .2, 3, 6, 9, 12, 14, 17, 20, 23]
			);
		}
		
		// 开始动画
		protected override function beginStarLine():void {
			// 添加星星连线动画
			for (var i:int = 0; i < 9; i++) {
				addStarLine(new StarLine(_points[i], _points[i + 1]));
			}
			addStarLine(new StarLine(_points[9], _points[6]));
		}
		
	}
	
}
