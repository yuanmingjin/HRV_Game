﻿package com.hrv.starry {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import com.greensock.*;
	
	/**
	 * 星星
	 * 
	 * 星星动画，主要就是两次放大的操作，可以通过设置firstScale、secondScale设置两次发大的倍数
	 * 
	 */
	public class Star63 extends MovieClip {
		public var timelineMax:TimelineMax;			// 动画序列组
		public var _firstScale:Number = 6;			// 第一次放大倍数
		public var _secondScale:Number = 4.5;		// 第二次放大倍数
		private var _alpha:Number = .2;				// 初始默认的透明度
		private var _defaultScale:Number = 1.5;		// 初始默认的倍数
		public var _duration:Number = 1.5;			// 每一次放大、缩小的时间
		
		public function Star63(defaultScale:Number = 1.5) {
			// constructor code
			// 创建一个来回播放的动画序列组：repeat重复次数，yoyo：来回播放
			timelineMax = new TimelineMax({repeat:int.MAX_VALUE, yoyo:true});
			this.scaleX = this.scaleY = _defaultScale = defaultScale;
			this.alpha = _alpha;
		}
		
		// 开始动画
		public function start():void {
			timelineMax.append(new TweenMax(this, _duration, {alpha:1, scaleX:firstScale, scaleY:firstScale, rotation:0}));
			timelineMax.append(new TweenMax(this, _duration, {alpha:_alpha, scaleX:defaultScale, scaleY:defaultScale}));
			timelineMax.append(new TweenMax(this, _duration, {alpha:1, scaleX:secondScale, scaleY:secondScale, rotation: -45 + Math.random() * 90}));
		}
		
		// 结束动画
		public function end():void {
			timelineMax.clear();
			TweenMax.to(this, 1, {alpha:0, onComplete:onComplete});
		}
		
		public function onComplete():void {
			this.parent.removeChild(this);
		}
		
		/// getter/setter
		public function get defaultScale():Number {
			return this._defaultScale;
		}
		
		public function set defaultScale(value:Number):void {
			this._defaultScale = value;
		}
		
		public function get firstScale():Number {
			return this._firstScale;
		}
		
		public function set firstScale(value:Number):void {
			this._firstScale = value;
		}
		
		public function get secondScale():Number {
			return this._secondScale;
		}
		
		public function set secondScale(value:Number):void {
			this._secondScale = value;
		}
	}
	
}
