﻿package com.hrv.starry {
	import flash.display.*;
	import com.greensock.events.TweenEvent;
	import com.greensock.TimelineMax;
	import com.greensock.TweenMax;
	import com.greensock.easing.Elastic;
	import com.hrv.event.GameEventDispatcher;
	import flash.events.Event;
	
	// 星座组合
	public class ConstellationGroup extends Sprite {
		private var timeLineMax:TimelineMax;
		private var constellationPositionBmp:Bitmap;	// 左侧星座缩略图
		private var _constellations:Array;				// 星座集合
		private var _constellationIndex:int;			// “活跃的”星座在星座集合中的下标		
		private var _constellationClss:Array;
		
		public var dispatcher:GameEventDispatcher = new GameEventDispatcher();  
		
		public function ConstellationGroup(constellationPositionCls:Class, constellationClss:Array) {
			// constructor code
			
			this._constellationClss = constellationClss;
			// 初始化左侧星座缩略图框
			constellationPositionBmp = new Bitmap(new constellationPositionCls as BitmapData);
			constellationPositionBmp.scaleX = constellationPositionBmp.scaleY = .57;
			constellationPositionBmp.x = 30;
			constellationPositionBmp.y = 70;
			addChild(constellationPositionBmp);
			
			//init();
		}
		
		public function init():void {
			if (_constellations != null) {
				for (var i:int = 0; i < _constellations.length; i++) {
					_constellations[i].removeMainlineAnimation();
				}
			}
			timeLineMax = new TimelineMax();
			_constellations = new Array();
			_constellationIndex = 0;
			
			/*var length: int = this.numChildren;
			var i: int = length;
			while (--i > -1) {
				var display:DisplayObject = this.getChildAt(i);
				if (this.contains(display))
					removeChild(display);
			}*/
			
			// 添加星座并开始动画
			for (i = 0; i < _constellationClss.length; i++) {
				var constellation:Constellation = _constellationClss[i] as Constellation;
				constellations.push(constellation);
				constellation.timeLineMax.addEventListener(TweenEvent.COMPLETE, onComplete);
				//constellation.timeLineMax.timeScale(2);	// 测试环境，2倍播放速度
			}
		}
		
		public function begin():void {
			if (constellations != null) {
				addChild(constellations[constellationIndex]);
				constellations[constellationIndex].begin();
				constellationIndex++;
			}
		}
		
		public function end():void {
			timeLineMax = null;
			for (var i:int = 0; i < constellations.length; i++) {
				constellations[i].timeLineMax.clear();
				/*constellations[i].timeLineMax = null;
				if (this.contains(constellations[i])) 
					removeChild(constellations[i]);*/
				//constellations[i] = null;
			}
			//_constellations = null;
			
		}
		
		/** 
		 * 单个星座动画完成之后
		 * 移动单个星座到左侧缩略图框当中
		 */
		private function onComplete(evt:TweenEvent):void {
			var constellation:Constellation = null;
			var $x:Number = 0;
			var $y:Number = 0;
			var $scale:Number = .2;
			var $rotation:Number = 0;
			for (var i:int = 0; i < constellations.length; i++) {
				if (evt.target == constellations[i].timeLineMax) {
					constellation = constellations[i];
					switch (i) {
						case 0:
							$x = -91; $y = 43;
							break;
						case 1:
							$x = -71; $y = 120;
							break;
						case 2:
							$x = -27; $y = 194; $scale = .17;
							break;
						case 3:
							$x = -60; $y = 322; $rotation = -27.8; $scale = .19; 
							break;
					}
				}
			}
			// 发大星座的动画
			timeLineMax.append(TweenMax.to(constellation, 1.5, 
					{
						scaleX: 1.05, 
						scaleY: 1.05,
						ease: Elastic.easeIn,
						onUpdate:onUpdate,
						onUpdateParams:[constellation, constellation.x, constellation.y, constellation.width, constellation.height]
					}
				)
			);
			// 移动星座
			timeLineMax.append(TweenMax.to(constellation, 1, 
					{
						scaleX: $scale, 
						scaleY: $scale,
						x: $x, 
						y: $y,
						rotation: $rotation,
						onComplete:onCompleteForSelf,
						onCompleteParams:[constellation]
					}
				)
			);
			
		}
		
		// 移动完星座，删除星座元件中的星星、星星连接线
		private function onCompleteForSelf(obj:Object):void {
			var constellation:Constellation = obj as Constellation;
			constellation.removeMainlineAnimation();
			
			if (constellationIndex < constellations.length) {
				addChild(constellations[constellationIndex]);
				constellations[constellationIndex].begin();
				constellationIndex++;
			} else {
				// 完成全部星座动画
				dispatcher.dispatchEvent(new Event("ConstellationGroupEnd"));
			}
		}
		
		// 放大星座过程中，动态改变元件注册点位置
		private function onUpdate(obj:Object, oldX:Number, oldY:Number, oldWidth:Number, oldHeight:Number):void {
			var constellation:Constellation = obj as Constellation;
			constellation.x = oldX - (constellation.width / 2 - oldWidth / 2);
			constellation.y = oldY - (constellation.height / 2 - oldHeight / 2);
		}
		
		public function get constellations():Array {
			return this._constellations;
		}
		
		public function get constellationIndex():int {
			return this._constellationIndex;
		}
		
		public function set constellationIndex(value:int) {
			return this._constellationIndex = value;
		}

	}
	
}
