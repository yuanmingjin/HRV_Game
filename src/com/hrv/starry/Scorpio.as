﻿package com.hrv.starry {
	import com.greensock.*;
	import com.greensock.easing.Linear;
	import flash.display.*;
	
	/** 
	 * 天蝎座
	 * 
	 * 星座动画的主线、动画序列包含“星星间连线的动画”，以及“星座图”出现或消失的动画
	 * 根据星星间连线的动画进度，增加或删除“闪烁的星星”元件
	 * 
	 */
	public class Scorpio extends Constellation {
		/**
		 * @param starCls 星星类
		 * @param libraCls 天秤座图像类
		 */ 
		public function Scorpio(starCls:Class, libraCls:Class) {
			// constructor code
			super(starCls, libraCls, 
				[
					{x:706, y:98},
					{x:726, y:145},
					{x:734, y:204},
					{x:738, y:242},
					{x:647, y:174},
					{x:610, y:194},
					{x:568, y:266},
					{x:544, y:351},
					{x:477, y:394},
					{x:421, y:374},
					{x:420, y:304},
					{x:378, y:351}
				],
				[0, 0.3, 2, 3, 4, 8, 12, 16, 20, 23, 27, 31]
			);
		}
		
		// 开始动画
		protected override function beginStarLine():void {
			// 添加星星连线动画
			for (var i:int = 0; i < 3; i++) {
				addStarLine(new StarLine(_points[i], _points[i + 1]));
			}
			addStarLine(new StarLine(_points[1], _points[4]));
			for (i = 4; i < _points.length - 1; i++) {
				addStarLine(new StarLine(_points[i], _points[i + 1]));
			}
			
		}

	}
	
}
