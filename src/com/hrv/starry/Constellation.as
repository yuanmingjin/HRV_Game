﻿package com.hrv.starry {
	import flash.display.*;
	import com.greensock.*;
	import com.greensock.easing.Linear;
	
	// 星座基类
	public class Constellation extends Sprite {

		// 创建星星的类
		protected var _starCls:Class;
		// 星座图类
		protected var _constellationImgCls:Class;
		// 星星
		protected var _stars:Array;
		// 星星连线集合
		protected var _lines:Array;
		// 星座图
		protected var _constellationImg:Bitmap;
		// 动画序列
		public var timeLineMax:TimelineMax;
		// 星星出现的坐标
		protected var _points:Array;
		// 星星出现的时间
		protected var _starsAppearedTime:Array;
		// 星星连线动画持续时间
		protected var duration:Number = 3;
		
		/**
		 * @param starCls 创建星星的类
		 * @param constellationImgCls 创建星座图的类
		 * @param points 星星的坐标集合
		 * @param starsAppearedTime 星星的出现时间的集合
		 * 
		 */
		public function Constellation(starCls:Class, constellationImgCls:Class, points:Array, starsAppearedTime:Array) {
			// constructor code
			if (starCls == null || constellationImgCls == null || points == null || starsAppearedTime == null)
				throw new Error("星座初始化失败，构造函数形参非法");
			if (points.length != starsAppearedTime.length)
				throw new Error("星星坐标或出现时间数组数量不匹配");
			this._starCls = starCls;
			this._constellationImgCls = constellationImgCls;
			this._stars = new Array();
			this._lines = new Array();
			this._starsAppearedTime = starsAppearedTime;
			// 根据点坐标，初始化“闪烁的星星”
			this._points = points;
			for (var i:int = 0; i < _points.length; i++) {
				var star:MovieClip = new starCls as MovieClip;
				star.x = _points[i].x;
				star.y = _points[i].y;
				_stars.push(star);
				star.firstScale = 3;		// 星星动画第一次放大倍数
				star.secondScale = 3;		// 第二次放大倍数
			}
			// 星座图片
			_constellationImg = new Bitmap(new constellationImgCls as BitmapData);
			_constellationImg.alpha = 0;
			addChild(_constellationImg);
			
			// 主线动画：星星连线、星座图动画组，不含星星闪烁的元件
			timeLineMax = new TimelineMax(
				{
					onUpdate:onUpdate
				}
			);
		}
		
		/**
		 * 删除主线动画，包含所有星星、星星间连线元件
		 */
		public function removeMainlineAnimation():void {
			timeLineMax.clear();
			for (var i:int = 0; i < _stars.length; i++) { 
				if (this.contains(_stars[i]))
					removeChild(_stars[i]);
			}
			for (i = 0; i < _lines.length; i++) {
				if (this.contains(_lines[i]))
					removeChild(_lines[i]);
			}
		}
		
		/**
		 * 开始动画
		 */
		public function begin():void {
			// 开始星星连线的动画，延迟到子类处理
			beginStarLine();
			// 星座图出现动画，也是动画序列的一部分
			timeLineMax.append(TweenMax.to(_constellationImg, duration, {alpha:1, ease:Linear.easeNone}));
		}
		
		/**
		 * 模板方法，由子类处理
		 * 由于每个星座的连线动画数量、顺序不一致，所以初始化的工作交给子类处理
		 */
		protected function beginStarLine():void {}
		
		/**
		 * 主线动画组更新的过程中，根据主线动画的运行时间，增加或者删除星星闪烁的元件
		 */
		protected function onUpdate():void {
			// 如果星星连线动画，处于倒播的阶段
			if (timeLineMax.reversed() == true) {
				// 依次删除每个星星
				if (_constellationImg.alpha == 0) {
					if (this.contains(_stars[_stars.length - 1])) {
						removeChild(_stars[_stars.length - 1]);
					}
				}
				for (var i:int = 0; i < (_stars.length - 1); i++) {
					if (timeLineMax.time() <= _starsAppearedTime[i + 1]) {
						if (this.contains(_stars[i])) {
							removeChild(_stars[i]);
						}
					}
				}
			} else {
				for (i = 0; i < _stars.length; i++) {
					if (timeLineMax.time() >= _starsAppearedTime[i]) {
						if (!this.contains(_stars[i])) {
							addChild(_stars[i]);
							_stars[i].start();
						}
					}
				}
			}
		}
		
		/**
		 * 添加星星中间的连线
		 */
		protected function addStarLine(line:StarLine):void {
			addChild(line);
			_lines.push(line);
			timeLineMax.append(TweenMax.to(line, duration, {scaleX: line.scale, ease:Linear.easeNone}));
		}
	}
	
}
