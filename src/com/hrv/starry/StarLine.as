﻿package com.hrv.starry {
	import flash.display.Sprite;
	
	/**
	 * 星星中间的连线
	 * 
	**/
	public class StarLine extends Sprite {
		private var _dx:Number = 0;			// 两点间x距离
		private var _dy:Number = 0;			// 两点间y距离
		private var _angle:Number = 0;		// 两点夹角的弧度
		private var _distance:Number = 0;	// 两点间距离
		private var _scale:Number = 0;		// 线段缩放的倍数
		
		public var _color:uint = 0xffffff;
		public var _alpha:Number = .6;
		public var _height:Number = 2;		// 线段的高度
		private var _width:Number = 1;		// 线段初始宽度，最好不要改动，利于计算

		public function StarLine(origin:Object, destination:Object) {
			// constructor code
			// 因为绘制线条后，放大、缩小（修改scale）时，线条会自动按比例缩放宽高
			// 所以使用绘制图形的方式绘图
			this.graphics.beginFill(_color, _alpha);
			this.graphics.drawRect(0, 0, _width, _height);
			this.graphics.endFill();
			// 线段的位置：第一个点的位置
			this.x = origin.x;
			this.y = origin.y;
			
			_dx = destination.x - origin.x;
			_dy = destination.y - origin.y;
			// 通过反正切函数计算两个点夹角的弧度
			_angle = Math.atan2(_dy, _dx);
			// 根据弧度计算出“线段”旋转的角度
			this.rotation = _angle * 180 / Math.PI;
			// 根据勾股定理，求出两点间距离
			_distance = Math.sqrt(_dx * _dx + _dy * _dy);
			// 计算“线段”应该缩放的倍数
			_scale = _distance / _width;
		}
		
		public function get scale():Number {
			return this._scale;
		}

	}
	
}
