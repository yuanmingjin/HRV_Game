﻿package com.hrv.starry {
	import com.greensock.*;
	import com.greensock.easing.Linear;
	import flash.display.*;
	
	/** 
	 * 射手座
	 * 
	 * 星座动画的主线、动画序列包含“星星间连线的动画”，以及“星座图”出现或消失的动画
	 * 根据星星间连线的动画进度，增加或删除“闪烁的星星”元件
	 * 
	 */
	public class Sagittarius extends Constellation {
		/**
		 * @param starCls 星星类
		 * @param libraCls 星座图像类
		 */ 
		public function Sagittarius(starCls:Class, libraCls:Class) {
			// constructor code
			super(starCls, libraCls, 
				[
					{x:600, y:142},
					{x:650, y:181},
					{x:682, y:182},
					{x:646, y:231},
					{x:674, y:254},
					{x:616, y:275},
					{x:608, y:227},
					{x:571, y:186},
					{x:533, y:206},
					{x:508, y:264},
					{x:508, y:328},
					{x:540, y:357},
					{x:550, y:327},
					{x:707, y:252},
					{x:737, y:200},
					{x:697, y:292},
					{x:653, y:328},
					{x:650, y:366},
					{x:726, y:309},
					{x:740, y:276},
					{x:784, y:265}
				],
				[0, 0.3, 2, 3, 4, 8, 12, 16, 20, 23, 27, 31, 34, 36, 39, 42, 45, 48, 51, 54, 57]
			);
		}
		
		// 开始动画
		protected override function beginStarLine():void {
			// 添加星星连线动画
			addStarLine(new StarLine(_points[0], _points[1]));
			addStarLine(new StarLine(_points[1], _points[2]));
			addStarLine(new StarLine(_points[1], _points[3]));
			
			for (var i:int = 3; i < 11; i++) {
				addStarLine(new StarLine(_points[i], _points[i + 1]));
			}
			addStarLine(new StarLine(_points[10], _points[12]));
			addStarLine(new StarLine(_points[4], _points[13]));
			addStarLine(new StarLine(_points[13], _points[14]));
			addStarLine(new StarLine(_points[13], _points[15]));
			addStarLine(new StarLine(_points[15], _points[16]));
			addStarLine(new StarLine(_points[16], _points[17]));
			addStarLine(new StarLine(_points[15], _points[18]));
			addStarLine(new StarLine(_points[18], _points[19]));
			addStarLine(new StarLine(_points[19], _points[20]));
		}

	}
	
}
