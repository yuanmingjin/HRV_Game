﻿package {
	import com.hrv.common.iMovieClip;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author yuanmingjin
	 */
	public class GameOption extends iMovieClip {
		private var _soundArray:Vector.<SoundChannel>;
		private var _soundEffectArray:Vector.<SoundChannel>;
		private var _isChangeDifficulty:Boolean;				// 是否可以改变游戏难度
		private var _difficulty:int;							// 难度
		private var _soundVolume:Number;						// 音量
		private var _isSound:Boolean;  							// 是否有声音
		private var _isSoundEffect:Boolean; 					// 是否有音效
		private var _isMute:Boolean;							// 是否静音
		private var _default:Dictionary;	// 默认状态
		
		public function GameOption(isChangeDifficulty:Boolean, 
								   soundArray:Vector.<SoundChannel> = null, 
								   soundEffectArray:Vector.<SoundChannel> = null,
								   soundVolume:Number = 1,
								   difficulty:int = 1,
								   isSound:Boolean = true,
								   isSoundEffect:Boolean = true,
								   isMute:Boolean = false) {
				
			super();
			this._default = new Dictionary();
			_isChangeDifficulty = isChangeDifficulty;
			_soundArray = soundArray;
			_soundEffectArray = soundEffectArray;
			this._default["soundVolume"] = this.soundVolume = soundVolume;
			this._default["difficulty"] = this.difficulty = difficulty;
			this._default["isSound"] = this.isSound = isSound;
			this._default["isSoundEffect"] = this.isSoundEffect = isSoundEffect;
			this._default["isMute"] = this.isMute = isMute;   
									   
			updateVolume();
			
			soundCheckBox.addEventListener(MouseEvent.CLICK, onClickHandler);
			soundEffectCheckBox.addEventListener(MouseEvent.CLICK, onClickHandler);
			muteCheckBox.addEventListener(MouseEvent.CLICK, onClickHandler);
									   
			applyBtn.addEventListener(MouseEvent.CLICK, onClickHandler);
			defaultBtn.addEventListener(MouseEvent.CLICK, onClickHandler);
		}
		
		private function onClickHandler(evt:MouseEvent):void {
			
			if (evt.currentTarget == soundCheckBox || evt.currentTarget == soundEffectCheckBox) {
				// 如果点击音乐、音效按钮
				// 设置音量
				_soundVolume = 1;
				// “静音”改为“未选中”状态
				if (this.isMute) this.isMute = false;
			} else if (evt.currentTarget == muteCheckBox) {
				// 设置音量为0
				_soundVolume = 0;
				// 如果点击静音按钮，音乐、音效改为“未选中”状态
				if (this.isSound) this.isSound = false;
				if (this.isSoundEffect) this.isSoundEffect = false;
			} else if (evt.currentTarget == defaultBtn) {
				this.soundVolume = this._default["soundVolume"];
				this.difficulty = this._default["difficulty"];
				this.isSound = this._default["isSound"];
				this.isSoundEffect = this._default["isSoundEffect"];
				this.isMute = this._default["isMute"]; 
			}
			// 如果音乐、音效按钮都未选中，应当处于静音状态
			if (!this.isSound && !this.isSoundEffect) {
				this.isMute = true;
			}
			
			updateVolume();
		}
		
		// 调整音量
		private function updateVolume():void {
			var i:int = -1;
			if (_soundArray == null || !isSound) {
				while (++i <  _soundArray.length) {
					var transform:SoundTransform = _soundArray[i].soundTransform;
					transform.volume = _soundVolume;
					_soundArray[i].soundTransform = transform;
				}
			}
			i = -1;
			if (_soundEffectArray == null || !isSoundEffect) {
				while (++i <  _soundEffectArray.length) {
					transform = _soundEffectArray[i].soundTransform;
					transform.volume = _soundVolume;
					_soundEffectArray[i].soundTransform = transform;
				}
			}
		}
		
		
		// 声音
		public function set isSound(value:Boolean):void
		{
			_isSound = value;
			soundCheckBox.selected = _isSound;
		}
		
		public function get isSound():Boolean
		{
			return _isSound = soundCheckBox.selected;
		}
		
		// 音效
		public function set isSoundEffect(value:Boolean):void
		{
			_isSoundEffect = value;
			soundEffectCheckBox.selected = _isSoundEffect;
		}
		
		public function get isSoundEffect():Boolean
		{
			return _isSoundEffect = soundEffectCheckBox.selected;
		}
		
		// 静音
		public function set isMute(value:Boolean):void
		{
			_isMute = value;
			muteCheckBox.selected = _isMute;
		}
		
		public function get isMute():Boolean
		{
			return _isMute = muteCheckBox.selected;
		}
		
		// 难度
		public function set difficulty(value:int):void
		{
			_difficulty = value;
			gameRadioGroup.clicked = _difficulty - 1;
		}
		
		public function get difficulty():int
		{
			return _difficulty = gameRadioGroup.clicked + 1;
		}
		
		public function set soundVolume(value:Number):void
		{
			_soundVolume = value;
		}
		
		public function get soundVolume():Number
		{
			return _soundVolume;
		}
		
		
	}

}