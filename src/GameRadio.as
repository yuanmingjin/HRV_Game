﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.net.URLRequest;
	
	
	public class GameRadio extends MovieClip {
		
		private var _mouseSound:Sound; 	// 鼠标声音
		private var _selected:Boolean = true;
		
		public function GameRadio() {
			// constructor code
			_mouseSound = new Sound();
			_mouseSound.load(new URLRequest("RadioButton.mp3"));
			this.addEventListener(MouseEvent.CLICK, onClickHandler);
		}
		
		private function onClickHandler(evt:MouseEvent):void {
			this.selected = !this._selected;
			_mouseSound.play();
		}
		
		public function set selected(selected:Boolean):void {
			
			this._selected = selected;
			selectedImg.visible = this.selected;
		}
		
		public function get selected():Boolean {
			return this._selected;
		}
		
		public function get mouseSound():Sound {
			return this._mouseSound;
		}
	}
	
}
