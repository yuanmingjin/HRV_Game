﻿package {
	import flash.display.*;
	import flash.events.ContextMenuEvent;
	import flash.events.*;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.LocalConnection;
	import flash.system.fscommand;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	import flash.utils.Timer;
	import gs.TweenGroup;
	import gs.TweenLite;
	import flash.external.ExternalInterface;
	import com.flashandmath.dg.display.*;
	import flash.net.URLRequest;
	import button.*;
	import button.menu.*;
	import flash.utils.Dictionary;
	import util.*;
	import com.hrv.common.*;
	import com.hrv.meihua.*;
	import com.hrv.component.*;


	public class MainForMeihua extends Sprite {

		private static const XML_URL: String = "../config.xml"; // 配置文件URL
		private static const COUNTDOWN_FLAG: int = 300;	// 倒计时300s（5分钟），结束标记
		private static const SNOW_MAX_NUM_PARTICLES = 50; // 雪最大数量

		private var screenWidth: int; // 宽
		private var screenHeight: int; // 高
		private var loading: Loading; // 硬件Loading对象
		private var backMask: BackMask; // 背景遮罩
		private var customXmlLoader: CustomXMLLoader; // 自定义读取XML配置文件
		private var popupEffect: PopupEffect; // 弹出框效果
		private var _difficulty: int = 1; // 游戏难度（默认是简单）
		private var _soundVolume: Number = 1; // 游戏音量（范围0~1）
		//private var _isFull: Boolean = false; // 是否全屏
		private var _isChangeDifficulty: Boolean = true; // 是否可选难度
		private var countDownTimer: Timer; // 游戏计时器
		private var isKey: Boolean; // 是否开启键盘

		private var totalScore: Number; // 总分数（高 + 中 + 低）
		private var availableScore: Number; // 可用的分数（高 + 中）
		private var sounds: Vector.<Sound>;// 声音
		private var channels: Vector.<SoundChannel>; // 声道
		private var soundEffects: Vector.<Sound>;// 音效
		private var channelEffects: Vector.<SoundChannel>;
		private var _backgroundSound:Sound;	// 背景音乐

		private var gameVideo: GameVideoLoader; // 视频剪辑
		private var motions: Vector.<int>; // 动作指令（高中低）数组
		private var isMotion: Boolean; // 是否可以进行（高中低）对应动画

		private var snow: SnowDisplay; // 下雪特效
		private var poemDisplay: PoemDisplay;	// 诗特效
		private var birdLoader: ElementLoader;		// 鸟特效
		private var gameMenu: GameMenu; // 导航条
		private var gameOption: GameOption; // 游戏选项设置
		private var gameEnd: GameEnd; // 游戏结束
		private var lightLoader: ElementLoader;		// 结束时，光特效

		private var _isSound: Boolean = true; // 是否开启背景音乐
		private var _isSoundEffect: Boolean = true; // 是否开启音效
		private var _isMute: Boolean = false; // 是否静音
		
		private var _entrance:EntranceBackground;	// 进入游戏画面
		private var hrvDisplayObject: HrvDisplayObject;		// 曲线显示
		private var hrvMask: Sprite;	// 曲线动画遮罩层
		private var caption:int;	// 0:完成训练  1:继续努力  2:成绩无效

		public function MainForMeihua(): void {
			//registerInterface();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		// 注册外部调用接口
		private function registerInterface(): void {
			ExternalInterface.addCallback("setHrv", setHrv);	// 设置HRV值
			ExternalInterface.addCallback("writeEp", writeEp);		// 写EP值
			//ExternalInterface.addCallback("detected", detected);
			ExternalInterface.addCallback("setDifficulty", setDifficulty);	// 设置难度
			
		}

		private function init(e: Event = null): void {

			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			var myContextMenu: ContextMenu = new ContextMenu();
			myContextMenu.hideBuiltInItems();
			var item: ContextMenuItem = new ContextMenuItem("", true);
			//item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, onRyanSelect);
			myContextMenu.customItems.push(item);
			this.contextMenu = myContextMenu;
			
			// 进入游戏画面
			_entrance = new EntranceBackground();
			_entrance.entranceBtn.addEventListener(MouseEvent.CLICK, onEntranceClickHandler);
			addChild(_entrance);
		}
		
		// 点击进入游戏后
		private function onEntranceClickHandler(evt:MouseEvent):void {
			_entrance.removeEventListener(MouseEvent.CLICK, onEntranceClickHandler);
			removeChild(_entrance);
			_entrance = null;
			
			// 开启作弊
			cheat();
			// 初始化数值数据
			initData();
			// 加载动画视频
			gameVideo = new GameVideoLoader(this, "meihua/MeihuaVideo.swf", "MeihuaVideo", onLoadComplete);
			lightLoader = new ElementLoader(this, "Light.swf", "LightDisplay", onLoadComplete);
		}
		
		// 加载视频资源完成后，回调此函数
		private function onLoadComplete(mcDefinition: String): void {
			if (mcDefinition == "MeihuaVideo") {	// 视频资源加载完成
				// 初始化计算一些数据
				gameVideo.calcData();
				addChild(gameVideo.mc);
				gameVideo.mc.gotoAndStop(1);
				// 初始化显示列表，并加载config.xml
				initDisplayConfig();
				initLoadData();
			} else if (mcDefinition == "LightDisplay") {	// 结束光效果加载完成
				lightLoader.mc.x = screenWidth / 2;
				lightLoader.mc.y = screenHeight / 2 - 30;
				lightLoader.mc.scaleX = 1.3;
				lightLoader.mc.scaleY = 1.3;
			} else if (mcDefinition == "BirdDisplay") {		// 鸟特效加载完成
				addChild(birdLoader.mc);
			}
		}

		// 初始化数据
		private function initData(): void {
			screenWidth = stage.stageWidth;
			screenHeight = stage.stageHeight;

			//_isChangeDifficulty = false;
			availableScore = 0;
			totalScore = 0;
			motions = new Vector.<int>();
			isMotion = true;
			
			sounds = new Vector.<Sound>();
			channels = new Vector.<SoundChannel>();
			_backgroundSound = new Sound();
			_backgroundSound.load(new URLRequest("meihua/BackgroundSound.mp3"));
			sounds.push(_backgroundSound);
			
			soundEffects = new Vector.<Sound>();
			channelEffects = new Vector.<SoundChannel>();
		}

		// 初始化显示列表
		private function initDisplayConfig(): void {

			// 添加导航菜单
			gameMenu = new GameMenu();
			gameMenu.y = screenHeight - gameMenu.height;
			gameMenu.startBtn.addEventListener(MouseEvent.CLICK, onNavigationClickHandler);
			//gameMenu.stopBtn.addEventListener(MouseEvent.CLICK, onNavigationClickHandler);
			gameMenu.quietBtn.addEventListener(MouseEvent.CLICK, onNavigationClickHandler);
			gameMenu.optionBtn.addEventListener(MouseEvent.CLICK, onNavigationClickHandler);
			addChild(gameMenu);
			gameMenu.timer.text = GameTimerText.getText(COUNTDOWN_FLAG);
			gameMenu.updateDifficulty(this._difficulty);
			
			hrvDisplayObject = new HrvDisplayObject(11, 5);
			hrvDisplayObject.x = 1112;
			//hrvDisplayObject.y = 13;
			gameMenu.addChild(hrvDisplayObject);
			
			hrvMask = new Sprite();
			hrvMask.graphics.beginFill(0xFFFFFF);
            hrvMask.graphics.drawCircle(1134, 35.5, 17.5); 
            hrvMask.graphics.endFill();
			gameMenu.addChild(hrvMask);
			hrvDisplayObject.mask = hrvMask;
			
			// 诗
			if (poemDisplay == null) {
				poemDisplay = new PoemDisplay();
				//poemDisplay.tweenGroup.pause();
				poemDisplay.x = 50;
				poemDisplay.y = 50;
				addChild(poemDisplay);
			} 

			// 背景遮罩
			backMask = new BackMask(0x000000, screenWidth, screenHeight);
			addChild(backMask);

			// Loading
			loading = new Loading();
			loading.x = screenWidth / 2;
			loading.y = screenHeight / 2;
			addChild(loading);

			// FPS
			/*var fps: FPSShow = new FPSShow();
			fps.x = 50;
			fps.y = 500;
			addChild(fps);*/
			
			soundEffects = new Vector.<Sound>();
			for (var i:int = 0; i < gameMenu.numChildren; i++) {
				var btn:BaseButton = gameMenu.getChildAt(i) as BaseButton;
				if (btn != null) {
					soundEffects.push(btn.mouseSound);
				}
			}
		}

		// 初始化加载数据
		private function initLoadData(): void {
			customXmlLoader = new CustomXMLLoader(XML_URL);
			customXmlLoader.addEventListener(CustomLoaderEvent.COMPLETE, customLoaderEventHandler);
		}

		/**
		 * 自定义事件监听处理
		 * @param	evt 自定义事件类型
		 */
		private function customLoaderEventHandler(evt: CustomLoaderEvent): void {
			if (evt.target is CustomXMLLoader) {
				// 默认难度——简单
				customXmlLoader.difficulty = 1;
				//detected(1);
				writeEp(1);
			}

			// 删除CustomXmlLoader
			customXmlLoader.removeEventListener(CustomLoaderEvent.COMPLETE, customLoaderEventHandler);
		}

		/**
		 * 检测ep值是否有效
		 *
		 */
		/*private function detected(ep: Number): void {
			
			if (!isNaN(ep) && ep >= 0) {
				if (loading != null) {
					removeChild(loading);
					loading = null;
					backMask.destroy();
				}
			}
		}*/
		
		// 初始化雪效果
		private function initSnow():void {
			snow = new SnowDisplay(screenWidth, screenHeight);
			snow.maxNumParticles = SNOW_MAX_NUM_PARTICLES;
			snow.maxParticleSize = 7;
			snow.randAccelFactorX = 0.5;
			snow.gravity = 1.0;
			snow.waitCount = 0;
			addChild(snow);
		}


		// 点击按钮处理
		private function onNavigationClickHandler(evt: MouseEvent): void {
			if (evt.currentTarget as StartButton) {

				if (gameMenu.startBtn.enabled) {
					if (gameEnd != null) {
						this.removeChild(gameEnd);
						gameEnd = null;
						gameVideo.mc.gotoAndStop(1);
					}
					if (birdLoader != null) {
						this.removeChild(birdLoader.mc);
						birdLoader = null;
					}
					if (poemDisplay != null) {
						poemDisplay.tweenGroup.restart();
						poemDisplay.tweenGroup.pause();
					}
					if (lightLoader.mc.parent) {
						this.removeChild(lightLoader.mc);
					}
					Game_Main();

					initSnow();
					
					gameMenu.stopBtn.addEventListener(MouseEvent.CLICK, onNavigationClickHandler);
				}
			} else if (evt.currentTarget as StopButton) {
				// 判断是否大于HRV监测标准长度
				caption = this.countDownTimer.currentCount > gameVideo.hrvStandardTime ? 1 : 2;
				Game_End();

			} else if (evt.currentTarget as QuietButton) {
				// 添加黑色遮罩
				backMask = new BackMask(0x000000, screenWidth, screenHeight);
				addChild(backMask);
				
				var messageBox:MessageBox = new MessageBox();
				messageBox.name = "quietGameMessageBox";
				addChild(messageBox);
				popupEffect = new PopupEffect(messageBox, screenWidth, screenHeight, 1);
				messageBox.confirmBtn.addEventListener(MouseEvent.CLICK, Game_Quit);
				messageBox.cancelBtn.addEventListener(MouseEvent.CLICK, Cancel_Game_Quit);
				//Game_Quit();
				
			} else if (evt.currentTarget as OptionButton) {
				
				var i:int = -1;
				while (++i < soundEffects.length) {
					channelEffects.push(soundEffects[i].play(0, 0, new SoundTransform(_soundVolume)));
				}
				
				if (gameMenu.optionBtn.enabled) {
					//_isPause = false;
					// 添加黑色遮罩
					backMask = new BackMask(0x000000, screenWidth, screenHeight);
					addChild(backMask); 
					// 游戏设置对话框
					gameOption = new GameOption(_isChangeDifficulty, channels, channelEffects, _soundVolume,
												this._difficulty, this._isSound, this._isSoundEffect, this._isMute);
					addChild(gameOption);
					popupEffect = new PopupEffect(gameOption, screenWidth, screenHeight, 1);
					gameOption.close.addEventListener(MouseEvent.CLICK, onPopupCloseHandler);
				}
			}
		}

		// 弹出窗口关闭
		private function onPopupCloseHandler(evt: MouseEvent): void {
			// 设置当中选择的难度
			this._difficulty = gameOption.difficulty;
			// 更新Menu中难度显示
			gameMenu.updateDifficulty(this._difficulty);
			// 音量
			this._soundVolume = gameOption.soundVolume;
			// 选择的声音
			this._isSound = gameOption.isSound;
			this._isSoundEffect = gameOption.isSoundEffect;
			this._isMute = gameOption.isMute;
			// 删除“游戏设置”窗口
			if (gameOption != null) {
				removeChild(gameOption);
				gameOption = null;
			}
			backMask.destroy();
			evt.target.removeEventListener(MouseEvent.CLICK, onPopupCloseHandler);
		}

		// 游戏主函数
		private function Game_Main(): void {

			customXmlLoader.difficulty = _difficulty;
			//trace(customXmlLoader.low);
			gameMenu.start();

			//_isChangeDifficulty = true;

			var i: int = -1;
			while (++i < sounds.length) {
				var channel: SoundChannel = sounds[i].play(0, int.MAX_VALUE, new SoundTransform(_soundVolume));
				channels.push(channel);
			}
			
			

			// 倒计时，间隔1s
			countDownTimer = new Timer(1000);
			countDownTimer.repeatCount = COUNTDOWN_FLAG;
			countDownTimer.reset();
			countDownTimer.start();
			countDownTimer.addEventListener(TimerEvent.TIMER, onTimerHandler);
			countDownTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerCompleteHandler);

			addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
		}

		// 监听动作指令数组，分配动画
		private function onEnterFrameHandler(evt: Event): void {
			// 视频当前帧
			var videoCurrentFrame: int = gameVideo.mc.currentFrame;
			var videoTotalFrames: int = gameVideo.mc.totalFrames;
			
			if (isMotion && motions.length > 0) {
				//trace(motions.length);
				isMotion = false;

				var heartState: int = motions.shift();
				// 移动到的帧
				var motionframe: int = 0;	
				if (heartState == 1) { // 低状态
					motionframe = videoCurrentFrame < gameVideo.epFrameInterval 
								? 0
								: videoCurrentFrame - gameVideo.epFrameInterval;
				} else if (heartState == 2) {  // 中状态
					motionframe = (videoCurrentFrame + gameVideo.epFrameInterval / 2) > videoTotalFrames
								? videoTotalFrames
								: (videoCurrentFrame + gameVideo.epFrameInterval / 2);
				} else if (heartState == 3) {  // 高状态
					motionframe = (videoCurrentFrame + gameVideo.epFrameInterval) > videoTotalFrames
								? videoTotalFrames
								: (videoCurrentFrame + gameVideo.epFrameInterval);
				}
				TweenLite.to(gameVideo.mc, gameVideo.epInterval, 
					{
						frame: motionframe,
						onComplete: motionComplete,
						onCompleteParams: [heartState],
						onUpdate: motionUpdate,
						onUpdateParams: [heartState]
					}
				);
			}
		}
		
		// 视频播放方向
		private var snowDirection:int = -1;
		//private var poemDirection:int = -1;
		
		// 动作更新
		private function motionUpdate(heartState:int) {
			
			// 如果到“雪”出现或消失的时间
			if (gameVideo.mc.currentFrame == gameVideo.snowTimeFlag) {
				if (heartState == 1) {
					snowDirection = 0;	// 倒退
				} else {
					snowDirection = 1;	// 前进
				}
			}
			
			if (snowDirection == 0) {
				if (snow == null) {
					initSnow();
					snow.maxNumParticles = 0;
				}
				if (snow.maxNumParticles <= SNOW_MAX_NUM_PARTICLES) {
					snow.maxNumParticles += 0.2;
				}
			} else if (snowDirection == 1) {
				if (snow != null) {
					if (snow.maxNumParticles > 0) {
						snow.maxNumParticles -= 0.2;
					} else {
						removeChild(snow);
						snow = null;
					}
				}
			} 
			
			// 诗
			if (gameVideo.mc.currentFrame >= gameVideo.poemTimeFlag &&
				gameVideo.mc.currentFrame <= (gameVideo.poemTimeFlag + poemDisplay.totalTime) * gameVideo.frameRate) {
				if (heartState == 1) {
					poemDisplay.tweenGroup.reversed(true);
				} else {
					poemDisplay.tweenGroup.reversed(false);
					poemDisplay.tweenGroup.resume();
				}
			}
			
			// 鸟
			if (gameVideo.mc.currentFrame == gameVideo.birdTimeFlag) {
				if (birdLoader == null) {
					birdLoader = new ElementLoader(this, "meihua/Bird.swf", "BirdDisplay", onLoadComplete);
				}
			}
			
		}
		
		// 动画完成之后
		private function motionComplete(heartState:int) {
			
			if (heartState == 2 || heartState == 3) {
				if (gameVideo.mc.currentFrame == gameVideo.mc.totalFrames) {
					caption = 0;
					Game_End();
				}
			} 
			isMotion = true;
		}

		/**
		* 外部写EP到flash中
		* @param ep 
		*/
		private function writeEp(ep: Number): void {

			//gameMenu.trace_text.text = customXmlLoader.high.toString();
			// 如果是无效的EP值，直接返回
			if (isNaN(ep) || ep < 0) return;
			// 如果没有开启作弊
			if (!isKey) {
				if (loading != null) {
					removeChild(loading);
					loading = null;
					backMask.destroy();
				}
				
				var heartState = -1;
				if (ep <= customXmlLoader.low) {
					heartState = 1;
				} else if (ep < customXmlLoader.high) {
					heartState = 2;
				} else {
					heartState = 3;
				}
				motions.push(heartState);

				// 计算分数，并改变协调状态显示
				calcScoreAndChangeState(heartState);
			}
		}
		
		/**
		* 外部设置程序难度
		* @param difficulty 游戏难度
		* @param isChangeDifficulty 难度是否可以在游戏中改变
		*/
		private function setDifficulty(difficulty: Number, isChangeDifficulty: Number): void {
			this._difficulty = difficulty;
			this._isChangeDifficulty = isChangeDifficulty == 0 ? false : true;
		}
		
		/**
		* 外部设置HRV值
		* @param hrv 心率变异性
		*/
		private function setHrv(hrv: Number): void {
			
		}

		// 倒计时器
		private function onTimerHandler(evt: TimerEvent): void {
			gameMenu.timer.text = GameTimerText.getText(COUNTDOWN_FLAG - this.countDownTimer.currentCount);
			// 测试HRV曲线
			this.hrvDisplayObject.draw(10 + Math.random() * 35);
		}
		
		// 倒计时结束
		private function onTimerCompleteHandler(evt: TimerEvent): void {
			caption = 1;
			Game_End();
		}

		// 开启作弊模式
		private function cheat(): void {
			isKey = false;
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyBoardHandler);
		}

		/**
		 * 按方向键
		 * 上：低状态
		 * 下：中状态
		 * 左：高状态
		 * 右：关闭作弊模式
		 */
		private function onKeyBoardHandler(evt: KeyboardEvent): void {
			var heartState: int = -1;
			if (evt.keyCode == Keyboard.UP) {
				heartState = 1;
				isKey = true;
				motions.push(heartState);
			} else if (evt.keyCode == Keyboard.DOWN) {
				heartState = 2;
				isKey = true;
				motions.push(heartState);
			} else if (evt.keyCode == Keyboard.LEFT) {
				heartState = 3;
				isKey = true;
				motions.push(heartState);
			} else if (evt.keyCode == Keyboard.RIGHT) {
				isKey = false;
			}
			//trace(motions.length);
			// 计算分数，并改变协调状态显示
			calcScoreAndChangeState(heartState);
		}

		// 计算分数，并改变协调状态显示
		private function calcScoreAndChangeState(heartState: int) {
			totalScore += 1;
			if (heartState == 1) { //低状态
				gameMenu.xietiaozhuangtai.gotoAndStop(2);
			} else if (heartState == 2) { //中状态
				availableScore += .5;
				gameMenu.xietiaozhuangtai.gotoAndStop(4);
			} else if (heartState == 3) { //高状态
				availableScore += 1;
				gameMenu.xietiaozhuangtai.gotoAndStop(5);
			} else { // 灰色状态
				gameMenu.xietiaozhuangtai.gotoAndStop(1);
			}
		}

		// 移除所有监听器
		private function removeAllListener(): void {
			var i: int = -1;
			while (++i < sounds.length) {
				if (channels[i] != null) {
					channels[i].stop();
					channels[i] = null;
					//sounds[i].close();
				}
				sounds[i] = null;
			}
			
			if (channelEffects != null) {
				i = -1;
				while (++i < channelEffects.length) {
					channelEffects[i].stop();
					channelEffects[i] = null;
				}
			}
			
			if (soundEffects != null) {
				i = -1;
				while (++i < soundEffects.length) {
					soundEffects[i] = null;
				}
			}
			
			// 倒计时
			countDownTimer.stop();
			countDownTimer.removeEventListener(TimerEvent.TIMER, onTimerHandler);
			countDownTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerCompleteHandler);

			removeEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
		}

		// 退出游戏
		private function Game_Quit(e:MouseEvent): void {
			var strResult: String = ExternalInterface.call("quietGame");
		}
		
		// 取消退出游戏对话框
		private function Cancel_Game_Quit(e:MouseEvent): void {
			var messageBox = this.getChildByName("quietGameMessageBox");
			if (messageBox != null)
				removeChild(messageBox);
			backMask.destroy();
			e.target.removeEventListener(MouseEvent.CLICK, Cancel_Game_Quit);
		}
		
		
		// 游戏结束
		private function Game_End(): void {
			gameMenu.timer.text = GameTimerText.getText(COUNTDOWN_FLAG - this.countDownTimer.currentCount);
			// 移除所有监听器
			removeAllListener();
			// 清除舞台上的元件
			var length: int = this.numChildren;
			var i: int = length;
			while (--i > -1) {
				var display:DisplayObject = this.getChildAt(i);
				if (this.birdLoader != null) {
					if (display == this.birdLoader.mc)
						continue;
				}
				if (display == gameVideo.mc)
					continue;
				if (display == this.poemDisplay) {
					//poemDisplay.tweenGroup.pause();
					poemDisplay.tweenGroup.clear();
					trace("诗");
					continue;
				}
				if (this.contains(display))
					removeChild(display);
			}
			
			// 光 效果
			if (caption == 0)	// 0:完成游戏
				addChild(lightLoader.mc);
			
			// 计算得分
			var score: int = 0;
			if (caption != 2) {	// 2: 成绩无效
				score = Math.floor((availableScore / totalScore) * 100);
			}
			// 添加游戏结束画面
			gameEnd = new GameEnd(caption, String(score), String(score));
			gameEnd.x = screenWidth / 2 - gameEnd.width / 2;
			gameEnd.y = screenHeight / 2 - gameEnd.height / 2;
			addChild(gameEnd);

			// 游戏结束，发送消息到外部程序
			/*var strResult: String = ExternalInterface.call("endGame", caption, score, 
				COUNTDOWN_FLAG - this.countDownTimer.currentCount);*/

			if (backMask) backMask = null;
			if (loading) loading = null;
			if (channels) channels = null;
			if (sounds) sounds = null;
			if (channelEffects) channelEffects = null;
			if (soundEffects) soundEffects = null;
			if (motions) motions = null;

			stage.removeEventListener(KeyboardEvent.KEY_UP, onKeyBoardHandler);
			doClearance();

			cheat();
			initData();
			initDisplayConfig();
			initLoadData();
		}

		// 垃圾回收机强制调用
		private function doClearance(): void {
			trace("clear");
			try {
				new LocalConnection().connect("foo");
				new LocalConnection().connect("foo");
			} catch (error: Error) {}
		}

	}
}