﻿package  
{
	import flash.events.Event;
	/**
	 * ...
	 * @author yuanmingjin
	 */
	public class CustomLoaderEvent extends Event
	{
		public static const COMPLETE:String = "加载完成";
		public static const PROCESSED_DATA:String = "处理数据完成";
		public static const ALREADY_CONNECT:String = "已经连接";
		
		private var _type:String;
		private var data:*;
		
		public function CustomLoaderEvent(type:String) 
		{
			_type = type;
			super(_type);
		}
		
		override public function clone():Event
		{
			return new CustomLoaderEvent(_type);
		}
		
		override public function toString():String
		{
			return formatToString("StageEvent", "type", "bubbles", "cancelable", "eventPhase");
		}
		
		public function set customParameter(value:int):void
		{
			data = value;
		}
		
		public function get customParameter():int
		{
			return data;
		}
	}

}