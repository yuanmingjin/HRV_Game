﻿package  {
	
	import flash.display.MovieClip;
	
	
	public class GameEnd extends MovieClip {
		
		private var endCaptions:Vector.<String> = Vector.<String>(["完成训练", "继续努力", "成绩无效"]);
		
		/**
		*/
		public function GameEnd(caption:int, highestScore:String, ultimateScore:String) {
			// constructor code
			this.caption.text = endCaptions[caption];
			this.highest.text = highestScore;
			this.ultimate.text = ultimateScore;
		}
	}
	
}
