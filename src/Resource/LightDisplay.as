﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	
	public class LightDisplay extends MovieClip {
		
		
		public function LightDisplay() {
			// constructor code
			this.addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
		}
		
		private function onEnterFrameHandler(evt:Event):void {
			if (this.currentFrame == this.totalFrames) {
				this.stop();
				this.rotation += 1;
			}
		}
	}
	
}
