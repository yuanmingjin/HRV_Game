﻿package com.hrv.starry {
	
	import flash.display.MovieClip;
	import gs.TweenGroup;
	import gs.TweenLite;
	import flash.events.Event;
	
	public class Star63 extends MovieClip {
		private var tweenLiteArray:Array;
		public var tweenGroup:TweenGroup;	// 动画序列组
		//public var rotate:int = 1;
		
		public function Star63() {
			// constructor code
			tweenLiteArray = new Array();
			
			//rotate = Math.round(Math.random()) == 0 ? -1 : 1;
			
			//this.rotation = Math.random() * 360 * 180 / Math.PI;
			
			//var tween0: TweenLite = new TweenLite(this, 1.5, {alpha:1, scaleX:3, scaleY:3});
			//tweenLiteArray.push(tween0);
			
			//var tween1: TweenLite = new TweenLite(this, 1.5, {alpha:0, scaleX:1, scaleY:1});
			//tweenLiteArray.push(tween1);
			
			//var tween2: TweenLite = new TweenLite(this, 1.5, {alpha:1, scaleX:3, scaleY:3, rotation: rotate * 45});
			//tweenLiteArray.push(tween2);
			
			//var tween3: TweenLite = new TweenLite(this, 1.5, {alpha:.3, scaleX:1, scaleY:1, rotation: 0});
			//tweenLiteArray.push(tween3);
			
			this.alpha = 0.2;
			this.scaleX = this.scaleY = 1.5;
			
			//tweenGroup = new TweenGroup(tweenLiteArray);
			tweenGroup = new TweenGroup();
			tweenGroup.align = TweenGroup.ALIGN_SEQUENCE;
			tweenGroup.yoyo = int.MAX_VALUE;
			//tweenGroup.addEventListener(Event.COMPLETE, doAllComplete);
			tweenGroup.splice(0, 0, new TweenLite(this, 1.5, {alpha:1, scaleX:6, scaleY:6, rotation:0}));
			tweenGroup.splice(1, 0, new TweenLite(this, 1.5, {alpha:0.2, scaleX:1.5, scaleY:1.5}));
			tweenGroup.splice(2, 0, new TweenLite(this, 1.5, {alpha:1, scaleX:4.5, scaleY:4.5, rotation: -45 + Math.random() * 90}));
		}
		
		public var testVar:int = 0;
		
		private function onRotateComplete():void {
			//trace(this.rotation);
			//rotate = (Math.round(Math.random()) == 0) ? -1 : 1;
			//trace(rotate);
		}
		
		/*private function doAllComplete(evt:Event):void {
			rotate = Math.round(Math.random()) > 1 ? -1 : 1;
		}*/
	}
	
}
