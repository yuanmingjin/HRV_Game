﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	
	public class BirdDisplay extends MovieClip {
		
		
		public function BirdDisplay() {
			// constructor code
			addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
		}
		
		private function onEnterFrameHandler(evt:Event):void {
			if (this.currentFrame == this.totalFrames) {
				this.gotoAndPlay(85);
			}
		}
	}
	
}
