﻿package 
{
	import fl.motion.ColorMatrix;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.ColorMatrixFilter;
	import flash.filters.GlowFilter;
	import com.hrv.common.ICustomDestroy;
	
	/**
	 * ...
	 * @author yuanmingjin
	 */
	public class PopupEffect extends Sprite implements ICustomDestroy
	{
		private var _type:int;
		private var _mc:DisplayObject;
		private var _width:Number;
		private var _height:Number;
		
		private var originBrightness:Number = 11;
		private var originContrast:Number = 465;
		private var count:int = 0;
		
		public function PopupEffect(mc:DisplayObject, width:Number, height:Number, type:int = 1) 
		{
			_type = type;
			_mc = mc;
			_width = width;
			_height = height;
			
			_mc.x = _width * .5 - _mc.width * .5;
			_mc.y = _height * .5 - _mc.height * .5;
			var filterObj:ColorMatrixFilter = new ColorMatrixFilter();
			var filterObj1:GlowFilter = new GlowFilter(0x999999, .8, 4, 4, 3);
			filterObj.matrix = new Array(originBrightness, 0, 0, 0, originContrast, 0, originBrightness, 0, 0, originContrast, 0, 0, originBrightness, 0, originContrast, 0, 0, 0, 1, 0);
			_mc.filters = [filterObj, filterObj1];
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onEnterFrame(evt:Event):void
		{
			_mc.filters = [];
			var filterObj:ColorMatrixFilter = new ColorMatrixFilter();
			var filterObj1:GlowFilter = new GlowFilter(0x999999, .8, 6, 6, 1);
			if ((originContrast - 0) > 0.01)
			{
				originContrast += (0 - originContrast) * .5;
			}
			else
			{
				originContrast = 0;
			}
			if ((originBrightness - 1) > 1)
			{
				originBrightness += (1 - originBrightness) * .2;
			}
			else
			{
				originBrightness = 1;
			}
			filterObj.matrix = new Array(originBrightness, 0, 0, 0, originContrast, 0, originBrightness, 0, 0, originContrast, 0, 0,originBrightness, 0, originContrast, 0, 0, 0, 1, 0);
			//filterObj.matrix = new Array(11, 0, 0, 0, 465, 0, 11, 0, 0, 465, 0, 0, 11, 0, 465, 0, 0, 0, 1, 0);
			_mc.filters = [filterObj, filterObj1];
		}
		
		public function destroy():void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		public function get displayObject():DisplayObject
		{
			return _mc;
		}
		
		
	}

}