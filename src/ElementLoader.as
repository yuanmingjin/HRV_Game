﻿package  {
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.system.LoaderContext;
	import flash.system.ApplicationDomain;
	import flash.net.URLRequest;
	import flash.events.IOErrorEvent;
	import fl.controls.ProgressBar;
	import flash.events.ProgressEvent;
	
	public class ElementLoader {

		private var _swfLoader: Loader;		// 加载swf  
		private var _mc: MovieClip;			// 影片剪辑  
		private var _container: Sprite;		// 容器
		private var _url: String;			// swf路径
		private var _mcDefinition:String;	// mc定义的名称
		
		public function ElementLoader(container: Sprite, url: String, 
									mcDefinition: String, $onLoadComplete: Function) {
			// constructor code
			this._container = container;
			this._url = url;
			this._mcDefinition = mcDefinition;

			_swfLoader = new Loader();
			 /*添加事件响应，在swf加载完毕后再获取里面的资源*/  
			_swfLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete($onLoadComplete));
			_swfLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
			_swfLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler); 
			 /*新建加载器上下文，再新建应用程序域*/  
			var context:LoaderContext = new LoaderContext();  
			context.applicationDomain = new ApplicationDomain();  
			/*开始加载swf*/  
			_swfLoader.load(new URLRequest(_url), context); 
		}
		
		private function onProgress(e:ProgressEvent):void {
			var loadpre:int = e.bytesLoaded / e.bytesTotal * 100;
			//trace(loadpre);
		}
		
		private function onLoadComplete(callback: Function): Function {
			return function(e:Event):void {
				var domain:ApplicationDomain = (e.currentTarget as LoaderInfo).applicationDomain;
			
				/*可以这样获取.swf中的影片剪辑*/  
				var cls:Class = domain.getDefinition(_mcDefinition) as Class;
				_mc = new cls() as MovieClip;
				//_container.addChild(_mc);
				
				callback.apply(this, [_mcDefinition]);
			}
		}
		
		private function ioErrorHandler(e:IOErrorEvent):void {
			trace(e.text + "\n" + e.type); 
		}
		
		public function get mc(): MovieClip {
			return this._mc;
		}
		
		public function get url(): String {
			return this._url;
		}
		
		public function get mcDefinition(): String {
			return this._mcDefinition;
		}

	}
	
}
