﻿package util {
	
	public class GameTimerText {

		public function GameTimerText() {
			// constructor code
		}
		
		public static function getText(countDown:int):String {
			var s: String = "00";
			var m: String = "0";
			var second: int = countDown % 60;
			if (second < 10)
				s = "0" + second;
			else
				s = String(second);

			var minute: int = (countDown / 60) >> 0;
			if (minute < 10)
				m = String(minute);

			//countDown--;
			return m + ":" + s;
		}

	}
	
}
