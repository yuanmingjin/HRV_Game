﻿package  {
	
	public class Cheat {
		
		private var _isKey:Boolean;	// 是否开启键盘监听

		public function Cheat(stage:Object) {
			// constructor code
			_isKey = false;
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyBoardHandler);
		}
		
		private function onKeyBoardHandler(evt: KeyboardEvent): void {
			var heartState: int = -1;
			if (evt.keyCode == Keyboard.UP) {
				heartState = 1;
				isKey = true;
				motions.push(heartState);
			} else if (evt.keyCode == Keyboard.DOWN) {
				heartState = 2;
				isKey = true;
				motions.push(heartState);
			} else if (evt.keyCode == Keyboard.LEFT) {
				heartState = 3;
				isKey = true;
				motions.push(heartState);
			} else if (evt.keyCode == Keyboard.RIGHT) {
				isKey = false;
			}
		}
		
		public function get isKey():Boolean {
			return _isKey();
		}
		
		public function destroy():void {
			stage.removeEventListener(KeyboardEvent.KEY_UP, onKeyBoardHandler);
			_isKey = false;
		}

	}
	
}
