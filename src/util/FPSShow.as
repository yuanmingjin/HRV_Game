﻿package
{
	import flash.display.Sprite
	import flash.utils.Timer;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.text.TextField;

	public class FPSShow extends Sprite
	{
		private var txt:TextField;
		private var count:int = 0;
		
		public function FPSShow()
		{
			init();
		}
		
		private function init()
		{
			txt = new TextField();
			txt.textColor = 0xff0000;
			txt.selectable = false;
			addChild(txt);
			var myTimer:Timer = new Timer(1000);
			myTimer.addEventListener("timer", timerHandler);
			this.addEventListener("enterFrame", countHandler);
			myTimer.start();
		}
		
		private function timerHandler(evt:TimerEvent)
		{	
			//Timer实例调用的方法
			txt.text = "FPS:" + count;
			//每隔1秒进行清零
			count = 0;
		}
		
		private function countHandler(evt:Event)
		{	
			//真循环调用的方法
			count++;//数值递加
		}
	}
} 