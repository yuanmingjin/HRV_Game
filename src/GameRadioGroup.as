﻿package {

	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	public class GameRadioGroup extends MovieClip {
		// 默认哪个被选中
		private var _clicked: int = 0;
		private var num: int = 0;

		public function GameRadioGroup() {
			// constructor code
			num = this.numChildren;
			var i: int = -1;
			while (++i < num) {
				this.getChildAt(i).addEventListener(MouseEvent.CLICK, radioClickHandler);
			}
			redraw();

		}

		public function radioClickHandler(evt: MouseEvent): void {
			var i: int = -1;
			while (++i < num) {
				var child = this.getChildAt(i);
				if (evt.target == child) {
					_clicked = i;
					child.selectedImg.visible = true;
				} else
					child.selectedImg.visible = false;
			}
		}
		
		private function redraw():void {
			var i: int = -1;
			while (++i < num) {
				var child = this.getChildAt(i);
				if (i == _clicked)
					child.selectedImg.visible = true;
				else
					child.selectedImg.visible = false;
				//child.addEventListener(MouseEvent.CLICK, radioClickHandler);
			}
		}
		
		public function get clicked():int {
			return this._clicked;
		}
		
		public function set clicked(value:int):void {
			this._clicked = value;			
			redraw();
			
		}
	}

}