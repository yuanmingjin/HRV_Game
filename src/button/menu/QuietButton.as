﻿package button.menu {
	
	import button.MenuButton;
	
	
	public class QuietButton extends MenuButton {
		
		
		public function QuietButton() {
			// constructor code
			super();
			tooltip.txt.text = "退出";
			tooltip.x = 148;
		}
	}
	
}
