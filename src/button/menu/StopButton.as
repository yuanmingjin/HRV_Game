﻿package button.menu {
	
	import button.MenuButton;
	
	
	public class StopButton extends MenuButton {
		
		
		public function StopButton() {
			// constructor code
			super();
			tooltip.txt.text = "结束";
			tooltip.x = 91;
		}
	}
	
}
