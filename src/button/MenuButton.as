﻿package button {
	import flash.events.MouseEvent;
	
	public class MenuButton extends BaseButton {
		protected var tooltip: ToolTip;

		public function MenuButton() {
			// constructor code
			tooltip = new ToolTip();
			tooltip.y = -35;
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOverHandler);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOutHandler);
		}
		
		private function onMouseOverHandler(evt:MouseEvent):void {
			if (super.enabled) {
				this.parent.addChild(tooltip);
				tooltip.name = "tooltip";
			}
		}
		
		private function onMouseOutHandler(evt:MouseEvent):void {
			if (this.parent.getChildByName("tooltip"))
				this.parent.removeChild(tooltip);
		}

	}
	
}
