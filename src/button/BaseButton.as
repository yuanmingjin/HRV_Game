﻿package button {
	
	import flash.display.SimpleButton;
	import flash.filters.ColorMatrixFilter;
	import flash.media.Sound;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	
	public class BaseButton extends SimpleButton {
		
		private var myElements_array:Array = [0.3, 0.59, 0.11, 0, 0, 0.3, 0.59, 0.11, 0, 0, 0.3, 0.59, 0.11, 0, 0, 0, 0, 0, 1, 0];
		private var myColorMatrix_filter:ColorMatrixFilter = new ColorMatrixFilter(myElements_array);
		
		private var _mouseSound:Sound; 	// 鼠标声音
		
		public function BaseButton() {
			// constructor code
			_mouseSound = new Sound();
			_mouseSound.load(new URLRequest("Button.mp3"));
			
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOverHandler);
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
		}
		
		private function onMouseOverHandler(evt:MouseEvent):void {
			if (super.enabled)
				_mouseSound.play();
		}
		
		private function onMouseDownHandler(evt:MouseEvent):void {
			if (super.enabled)
				_mouseSound.play();
		}
		
		override public function set enabled(value:Boolean):void {
			super.enabled = value;
			value == false ? this.filters = [myColorMatrix_filter] : this.filters = null;
		}
		
		public function get mouseSound():Sound {
			return this._mouseSound;
		}
	}
	
}
