﻿package  {
	import flash.geom.Point;
	import flash.display.Sprite;
	
	public class HrvDisplayObject extends Sprite {
		private var points: Vector.<Point>;
		private var _count: int;		// 显示点的个数
		private var _distanceX: Number;	// x轴距离
		private var _alpha: Number;		// 笔刷透明度
		
		
		public function HrvDisplayObject(count: int = 5, distanceX: Number = 10/*, $alpha: Number = 1*/) {
			// constructor code
			points = new Vector.<Point>();
			this._count = count;
			this._distanceX = distanceX;
			//this._alpha = $alpha;
		}
		
		// 绘制HRV曲线
		public function draw($y: Number):void {
			points[points.length] = new Point(0, $y);
			
			this.graphics.clear();
			this.graphics.lineStyle(1, 0xffffff, .5);
			if (points.length >= this.count) {
				points.shift();
			}
			
			this.graphics.moveTo(0, points[0].y);
			var len:int = points.length - 1;
			for (var i:int = 1; i < len; i++) {
				this.graphics.lineTo(i * this.distanceX, points[i].y);
			}
		}
		
		public function get count(): int {
			return _count;
		}
		
		public function set count(value: int) {
			return _count = value;
		}
		
		public function get distanceX(): Number {
			return _distanceX;
		}
		
		public function set distanceX(value: Number) {
			return _distanceX = value;
		}
	}
	
}
