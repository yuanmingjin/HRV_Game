﻿package
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.IEventDispatcher;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import com.hrv.common.ICustomDestroy;
	
	/**
	 * ...
	 * @author yuanmingjin
	 */
	public class CustomXMLLoader extends EventDispatcher implements ICustomDestroy
	{
		private var spcsXmlLoader:URLLoader;
		
		private var _simplLow:Number = 0;		// xml配置，简单
		private var _simplHigh:Number = 0;
		private var _middingLow:Number = 0;		// xml配置，中等
		private var _middingHigh:Number = 0;
		private var _hardLow:Number = 0;		// xml配置，困难
		private var _hardHigh:Number = 0;
		
		private var _low:Number = 0;
		private var _high:Number = 0;
		
		/**
		 * 
		 * @param	url 自定义URL
		 */
		public function CustomXMLLoader(url:String = null):void
		{
			spcsXmlLoader = new URLLoader(new URLRequest(url));
			configureListeners(spcsXmlLoader);
		}
		
		private function configureListeners(dispatcher:IEventDispatcher):void 
		{
            dispatcher.addEventListener(Event.COMPLETE, onXmlLoad);
			dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
        }
		
		private function onXmlLoad(evt:Event):void
		{
			var spcsXml:XML = new XML(spcsXmlLoader.data);
			var spcsXmlObj:XML = spcsXml.children()[0];
			
			_simplLow = spcsXmlObj.children()[0].attribute("low");
			_simplHigh = spcsXmlObj.children()[0].attribute("hight");
			
			_middingLow = spcsXmlObj.children()[1].attribute("low");
			_middingHigh = spcsXmlObj.children()[1].attribute("hight");
			
			_hardLow = spcsXmlObj.children()[2].attribute("low");
			_hardHigh = spcsXmlObj.children()[2].attribute("hight");
			
			dispatchEvent(new CustomLoaderEvent(CustomLoaderEvent.COMPLETE));
		}
		
		// 错误处理
		private function ioErrorHandler(evt:IOErrorEvent):void
		{
			trace("ioError: " + evt);
		}
		
		private function securityErrorHandler(evt:SecurityError):void
		{
			trace("securityError:" + evt);
		}
		
		/**
		 * 删除对象
		 */
		public function destroy():void
		{
			destroyListener(spcsXmlLoader);
			spcsXmlLoader = null;
		}
		
		/**
		 * 删除监听
		 */
		private function destroyListener(dispatcher:IEventDispatcher):void 
		{
			dispatcher.removeEventListener(Event.COMPLETE, onXmlLoad);
			dispatcher.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			dispatcher.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		}
		
		/**
		 * 设置难度
		 */
		public function set difficulty(value:int):void
		{
			switch(value)
			{
				case 1:
					_low = _simplLow as Number; 
					_high = _simplHigh as Number; 
					break;
				case 2:
					_low = _middingLow as Number; 
					_high = _middingHigh as Number;
					break;
				case 3:
					_low = _hardLow as Number; 
					_high = _hardHigh as Number; 
					break;
				default: 
					_low = _simplLow as Number; 
					_high = _simplHigh as Number;
			}
		}
		
		public function get low():Number
		{
			return _low;
		}
		
		public function get high():Number
		{
			return _high;
		}
	}
}