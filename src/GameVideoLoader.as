﻿package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.display.Loader;
	import flash.system.LoaderContext;
	import flash.system.ApplicationDomain;
	import flash.net.URLRequest;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	
	public class GameVideoLoader extends ElementLoader {
		
		private var _videoTotalTime:int = 48;	// 视频长度(s)
		private var _frameRate:int;				// 帧频
		private var _hrvStandardTime:int = 75;	// hrv监测的标准时长(s)
		private var _snowTimeFlag:int = 19;		// “雪”出现或消失时间(s)
		private var _poemTimeFlag:int = 30;
		private var _poemReversedTimeFlag:int;
		private var _birdTimeFlag:int = 70;
		private var _epInterval:int = 5;		// ep间隔5s
		private var _epFrameInterval:int;		// ep每5s走多少帧（高状态）
		
		//private var _videoLoader:Loader;		//加载swf  
		//private var _mcVideo:MovieClip;		//整个视频的影片剪辑  
		//private var _container: Sprite;
		
		public function GameVideoLoader(container: Sprite, url: String, mcDefinition: String, $onLoadComplete: Function):void {
			super(container, url, mcDefinition, $onLoadComplete);
		}
		//{
		//	this._container = container;
		//	_videoLoader = new Loader();
		//	 /*添加事件响应，在swf加载完毕后再获取里面的资源*/  
		//	_videoLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete($onLoadComplete));  
		//	 /*新建加载器上下文，再新建应用程序域*/  
		//	var context:LoaderContext = new LoaderContext();  
		//	context.applicationDomain = new ApplicationDomain();  
		//	/*开始加载swf*/  
		//	_videoLoader.load(new URLRequest("Video.swf"), context); 
		//}
		//
		//private function onLoadComplete(callback: Function): Function {
		//	return function(e:Event):void {
		//		var domain:ApplicationDomain = (e.currentTarget as LoaderInfo).applicationDomain;
		//	
		//		/*可以这样获取bag.swf中的bag_panel影片剪辑*/  
		//		var cls:Class = domain.getDefinition("MeihuaVideo") as Class;
		//		_mcVideo = new cls() as MovieClip;
		//		_container.addChild(_mcVideo);
		//		
		//		callback.apply(null);
		//	}
		//}
		
		// 初始化之后计算一些数据
		public function calcData():void {
			_frameRate = super.mc.totalFrames / _hrvStandardTime;
			_epFrameInterval = _frameRate * _epInterval;
		}
		
		//private function onLoadComplete(e:Event):void  
		//{  
		//	/*获得加载器的应用程序域*/  
		//	var domain:ApplicationDomain = (e.currentTarget as LoaderInfo).applicationDomain;
		//	
		//	/*可以这样获取bag.swf中的bag_panel影片剪辑*/  
		//	var cls:Class = domain.getDefinition("MeihuaVideo") as Class;  
		//	_mcVideo = new cls() as MovieClip;  
		//	_container.addChild(_mcVideo);
		//	
		//	_frameRate = this._mcVideo.totalFrames / _hrvStandardTime;
		//	_epFrameInterval = _frameRate * _epInterval;
		//}
		
		public function get snowTimeFlag():int {
			return _snowTimeFlag * _frameRate;
		}
		
		public function get poemTimeFlag():int {
			return _poemTimeFlag * _frameRate;
		}
		
		public function get poemReversedTimeFlag():int {
			return _poemReversedTimeFlag;
		}
		
		/*public function set poemReversedTimeFlag(value:int):void {
			this._poemReversedTimeFlag = value;
		}*/
		
		public function get birdTimeFlag():int {
			return _birdTimeFlag * _frameRate;
		}
		
		public function get hrvStandardTime():int {
			return _hrvStandardTime;
		}
		
		public function get epInterval():int {
			return _epInterval;
		}
		
		public function get epFrameInterval():int {
			return _epFrameInterval;
		}
		
		/*public function get mcVideo():MovieClip {
			return this._mcVideo;
		}*/
		
		public function get frameRate():int {
			return this._frameRate;
		}
	}
}