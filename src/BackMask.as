﻿package
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import com.hrv.common.ICustomDestroy;
	
	/**
	 * ...
	 * @author yuanmingjin 
	 */
	public class BackMask extends Sprite implements ICustomDestroy
	{
		private var _color:uint;
		private var _width:Number;
		private var _height:Number;
		private var _alpha:Number = 0;
		private var _originAlpha:Number = 0;
		private var _targetAlpha:Number = .5;
		private var _alphaSpeed:Number = .06;
		private var _flag:Boolean = true;
		

		
		public function BackMask(color:uint = 0x000000, width:Number = 800, height:Number = 600)
		{
			this._color = color;
			this._width = width;
			this._height = height;
			draw();
			//addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);	
		}
		
		private function onEnterFrameHandler(evt:Event):void
		{
			if (_flag)
			{
				if (_alpha < _targetAlpha)
				{
					_alpha += _alphaSpeed;
					draw();
				}
			}
			else
			{
				if (_alpha > _originAlpha)
				{
					_alpha -= _alphaSpeed;
					draw();
				}
				else
				{
					removeEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
					if (parent) parent.removeChild(this);
				}
			}
		}
		
		private function draw():void
		{
			graphics.clear();
			graphics.beginFill(_color, .5);
			//graphics.beginFill(_color, _alpha);
			graphics.drawRect(0, 0, _width, _height);
			graphics.endFill();
		}
		
		public function destroy():void
		{
			if (parent) parent.removeChild(this);
			//_flag = false;
		}
	}
}