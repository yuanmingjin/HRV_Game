﻿package  {
	
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	
	
	public class GameMenu extends MovieClip {
		
		
		public function GameMenu() {
			// constructor code
			init();
		}
		
		public function init(difficulty:String = "简单"):void {
			this.xietiaozhuangtai.stop();
			this.difficulty.text = difficulty;
			this.startBtn.enabled = true;
			this.stopBtn.enabled = false;
		}
		
		public function start():void {
			this.startBtn.enabled = false;
			this.stopBtn.enabled = true;
			this.optionBtn.enabled = false;
		}
		
		public function end():void {
			this.startBtn.enabled = true;
			this.stopBtn.enabled = false;
			this.optionBtn.enabled = true;
		}
		
		public function updateDifficulty(difficulty:int) {
			var str = "";
			if (difficulty == 1) {
				str = "简单";
			} else if (difficulty == 2) {
				str = "中等";
			} else if (difficulty == 3) {
				str = "困难";
			}
			this.difficulty.text = str;
		}
		
	
	}
	
}
